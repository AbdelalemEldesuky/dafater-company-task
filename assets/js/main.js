$(document).ready(function () {
  let service = [
    {
      description: "Service Fee",
      qty: 1,
      unitPrice: 200.0,
      amount: 200.0,
    },
    {
      description: "Labor: 5 hours at $75/hr",
      qty: 2,
      unitPrice: 20.0,
      amount: 375.0,
    },
    {
      description: "Service Fee",
      qty: 4,
      unitPrice: 50.0,
      amount: 375.0,
    },
    {
      description: "Labor: 2 hours ",
      qty: 6,
      unitPrice: 90.0,
      amount: 375.0,
    },
  ];
  var subTotal = 0;
  var taxRate = 4.25;
  var tax;
  let htmlService = "";
  for (i = 0; i <= service.length - 1; i++) {
    htmlService += `
      <tr>
        <td> ${service[i].description} </td>
        <td> ${service[i].qty} </td>
        <td> ${service[i].unitPrice} </td>
        <td> ${service[i].qty * service[i].unitPrice}</td>  
        </tr>
        `;
    subTotal += service[i].qty * service[i].unitPrice;
  }
  document.getElementById("ServiceID").innerHTML = htmlService;
  document.getElementById("subTotal").innerHTML = subTotal;
  document.getElementById("taxRate").innerHTML = taxRate;
  console.log("dddddddddddddddd", taxRate);

  tax = subTotal * (taxRate / 100);
  tax = parseFloat(tax.toFixed(2));
  console.log("jjjjjjjjjjjjjjjjjjj", tax);
  var total = subTotal + tax;
  document.getElementById("tax").innerHTML = tax;
  document.getElementById("total").innerHTML = total.toFixed(2);
});
